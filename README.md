Example of running:

* Run celery worker

You need to have  `RabbitMQ` running and set `CELERY_BROKER` environment variable. Also, `CELERY_BACKEND` environment variable is needed to specify the access to the results backend. For example:
```
export CELERY_BROKER="amqp://localhost:5672"
export CELERY_BACKEND="db+sqlite:///results.db"
```
Then, you can run the worker with e.g.:
```
poetry run celery -A lbprschedule worker -Q default  --loglevel=debug
```

* Requesting a test

With the worker running, one can request a test:
```
poetry run request_test --options Moore_hlt2_fastest_reco --slot lhcb-master --slot_id 1332
```
