__version__ = "0.1.0"

import os
from celery import Celery
from celery.utils.log import get_task_logger
from celery.signals import worker_process_init
from kombu import Queue

app = Celery(
    __name__, broker=os.environ["CELERY_BROKER"], backend=os.environ["CELERY_BACKEND"],
)

logger = get_task_logger(__name__)

from .tasks import run_lhcbpr_test
from .beat import setup_periodic_tasks

app.conf.result_extended = True
app.conf.task_acks_late = True
app.conf.task_track_started = True
app.conf.worker_prefetch_multiplier = 1
app.conf.event_serializer = "json"
app.conf.task_serializer = "json"
app.conf.task_compression = "gzip"
app.conf.result_serializer = "json"
app.conf.result_compression = "gzip"
app.conf.accept_content = ["json"]
app.conf.result_accept_content = ["json"]

app.conf.task_default_queue = "default"
app.conf.task_queues = (
    Queue("default", routing_key="task.#"),
    Queue("perf", routing_key="perf.#"),
    Queue("perf-centos7", routing_key="perf-centos7.#"),
    Queue("perf-centos7-timing", routing_key="perf-centos7-timing.#"),
    Queue("veloupgrade", routing_key="veloupgrade.#"),
    Queue("throughput-avx512", routing_key="throughput-avx512.#"),
)

# global variable to store the path to worker process directory
# the value is set at worker process initialisation
worker_process_dir = ""


@worker_process_init.connect()
def configure_worker(signal=None, sender=None, **kwargs):
    global worker_process_dir
    worker_process_dir = os.path.join(
        os.path.dirname(os.environ["CELERY_LOG_FILE"]),
        os.path.basename(os.environ["_MP_FORK_LOGFILE_"]).split(".")[0],
    )
    if worker_process_dir:
        os.makedirs(worker_process_dir, exist_ok=True)
