from . import app
from .tasks import check_daily_tests, check_new_builds


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(
        freq := 60.0 * 60 * 24,
        check_daily_tests.s(queue="default"),
        name=f"get daily lhcbpr tests every {freq}s",
        expires=freq * 2,
    )
    sender.add_periodic_task(
        freq := 60.0,
        check_new_builds.s(queue="default", freq=freq),
        name=f"check for new builds ands run lhcbpr tests if needed, every {freq}s",
        expires=60 * 60 * 24,
    )
