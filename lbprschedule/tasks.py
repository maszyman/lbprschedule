import os
from os.path import join
import re
import logging
from time import time
from subprocess import run, PIPE
from celery.backends.database import session_cleanup
from sqlalchemy.orm import defer
from sqlalchemy import and_
from datetime import timedelta, datetime
from .utils import get_test_schedule, get_test_cmds, CondaEnv
from . import app

@app.task(bind=True)
def run_lhcbpr_test(self, *args, **kwargs):
    print(f"Running {kwargs}")
    from . import worker_process_dir
    # with CondaEnv(base_dir=worker_process_dir,)():
    with CondaEnv()() as wenv:
        for step, cmd in kwargs.items():
            if cmd[0] in ["lb-run", "lbn-install"]:
                kwargs[step][0] = os.path.join(wenv.bin_dir, kwargs[step][0])
        res = run(kwargs["install"], capture_output=True, env=wenv.env)
        print(res.stdout.decode())
        print(res.stderr.decode())
        if not res.returncode:
            res = run(kwargs["lbrun"], capture_output=True, env=wenv.env)
            print(res.stdout.decode())
            print(res.stderr.decode())
    return f"lhcbpr test done"


@app.task(bind=True)
def check_daily_tests(self, *args, **kwargs):
    tests = get_test_schedule()
    tests = {
        k: v
        for k, v in tests.items()
        if v["when"] == "daily"
        or (isinstance(v["when"], list) and datetime.now().strftime("%a") in v["when"])
    }

    for _, test in tests.items():
        for test_cmd in get_test_cmds(test):
            print(f"print{test_cmd}")
            logging.info(test_cmd)
            run_lhcbpr_test.apply_async(
                queue="default", kwargs=test_cmd, expires=60 * 60,
            ),


NIGHTLIES_CVMFS = "/cvmfs/lhcbdev.cern.ch/nightlies/"


@app.task(bind=True)
def check_new_builds(self, *args, **kwargs):
    tests = get_test_schedule()
    tests = {k: v for k, v in tests.items() if v["when"] == "each_build"}
    for _, test in tests.items():
        for slot in test["slots"]:
            for platform in test["platforms"]:
                slot_path = os.path.join(NIGHTLIES_CVMFS, slot)
                try:
                    bids = [
                        bid
                        for bid in os.listdir(slot_path)
                        if time()
                        - os.stat(
                            os.path.join(
                                slot_path, bid, test["project"], "InstallArea", platform
                            )
                        ).st_mtime
                        < kwargs["freq"]
                        and not os.path.islink(os.path.join(slot_path, bid))
                    ]
                except FileNotFoundError:
                    logging.warning(
                        f"Slot {slot} requested for the test was not found on cvmfs"
                    )
                    bids = []
                for bid in bids:
                    for test_cmd in get_test_cmds(test, slot, bid, platform):
                        print(f"print{test_cmd}")
                        logging.info(test_cmd)
                        run_lhcbpr_test.apply_async(
                            queue="default", kwargs=test_cmd, expires=60 * 60,
                        ),


