import os
import yaml
import requests
from os.path import join, dirname, basename
from functools import lru_cache
import logging
from contextlib import contextmanager
from subprocess import run, CalledProcessError
from urllib.request import urlopen
from shutil import copyfileobj
from tempfile import NamedTemporaryFile


logger = logging.getLogger(__name__)


def worker_dir():
    worker_dir = join(
        dirname(os.environ["CELERY_LOG_FILE"]),
        basename(os.environ["_MP_FORK_LOGFILE_"]).split(".")[0],
    )
    os.makedirs(worker_dir, exist_ok=True)
    return worker_dir


def called_process_to_json(proc):
    return {
        "args": proc.args,
        "returncode": proc.returncode,
        "stdout": proc.stdout.decode(),
        "stderr": proc.stderr.decode(),
    }


def get_test_environment():
    # pull tests environment
    environment = {}
    try:
        r = requests.get(
            "https://gitlab.cern.ch/maszyman/lbprschedule"
            f"/-/raw/master/lbprschedule/data/environment.yaml",
            timeout=5,
        )
        r.raise_for_status()
        environment = yaml.safe_load(r.text)
    except Exception:
        logger.exception("Failed to get tests environment.yaml")
    return environment


def get_test_schedule():
    # pull tests schedules
    scheduled_tests = {}
    tested_projects = ["Moore"]
    for project in tested_projects:
        try:
            r = requests.get(
                "https://gitlab.cern.ch/maszyman/lbprschedule"
                f"/-/raw/master/lbprschedule/data/{project}.yaml",
                timeout=5,
            )
            r.raise_for_status()
            file_defs = yaml.safe_load(r.text)
            try:
                defs = file_defs.pop("defaults")
            except KeyError:
                defs = {}
            for key in file_defs.keys():
                scheduled_tests[key] = defs | file_defs[key]
        except Exception as exc:
            logger.exception(f"Failed to get tests schedule")

    return scheduled_tests


def get_slots(tests):
    ss = []
    _ = [
        ss.append(slot)
        for slots in [td["slots"] for _, td in tests.items()]
        for slot in slots
    ]
    return set(ss)


class CondaEnv:
    """
    Class creating conda environment
    """

    def __init__(
        self, base_dir=os.getcwd(), environment=None, auth=True,
    ):
        self.base_dir = base_dir
        self.prefix = os.path.join(self.base_dir, "lhcbpr-env")
        self.environment = environment or get_test_environment()
        self.conda_cmd = f"{self.base_dir}/miniconda/bin/conda"
        if not os.path.exists(self.conda_cmd):
            logger.debug("Installing conda")
            with urlopen(
                "https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh"
            ) as response:
                with open(f"{self.base_dir}/miniconda.sh", "wb") as outfile:
                    copyfileobj(response, outfile)
            init_conda_cmd = [
                "bash",
                f"{self.base_dir}/miniconda.sh",
                "-b",
                "-u",
                "-p",
                f"{self.base_dir}/miniconda",
            ]
            try:
                run(init_conda_cmd, check=True)
            except CalledProcessError as err:
                logger.error("Couldn't setup conda installation")
                raise EnvironmentError(err.stderr.decode())
            os.remove(f"{self.base_dir}/miniconda.sh")

        logger.debug(f"Using conda from: {self.conda_cmd}")
        self.env_yml = f"{self.base_dir}/environment.yml"
        self.env = dict(os.environ)
        self.auth = auth

    @contextmanager
    def __call__(self):
        """
        Calling class object yields context manager, which installs the requirements
        """

        try:
            # check if there's existing environment.yml
            # and if so, compare with the requested environment
            # no need for creating the new conda env
            # if they're the same
            old_env = {}
            with open(self.env_yml, "r") as old_env_file:
                old_env = yaml.safe_load(old_env_file)
            assert old_env == self.environment
            # make sure there's at least python executable in the environment
            assert os.path.exists(f"{self.prefix}/bin/python")
            logger.debug("Reusing the existing environment")
        except (FileNotFoundError, AssertionError):
            logger.debug("Creating conda environment")
            with open(self.env_yml, "w") as envfile:
                envfile.write(yaml.safe_dump(self.environment))
            env_cmd = [
                self.conda_cmd,
                "env",
                "create",
                "--file",
                self.env_yml,
                "--prefix",
                self.prefix,
                "--force",
            ]
            try:
                run(env_cmd, check=True, capture_output=True)
            except (OSError, CalledProcessError) as err:
                logger.error("Creating conda environment failed")
                raise EnvironmentError(err.stderr.decode())

        self.bin_dir = f"{self.prefix}/bin/"
        self.python = f"{self.prefix}/bin/python"

        if self.auth:
            _krb_token = NamedTemporaryFile()
            try:
                run(
                    ["kinit", "-c", _krb_token.name, os.environ["LBPR_USER"]],
                    input=os.environ["LBPR_PASS"].encode(),
                    check=True,
                )
            except (KeyError, CalledProcessError) as err:
                logger.warning(
                    f"Could not perform kerberos authentication"
                )
        self.env["KRB5CCNAME"] = f"FILE:{_krb_token.name}"
        yield self


def valid_test_definition(test_def):
    if not isinstance(test_def, dict):
        return False
    string_keys = ["project", "command", "host_label", "options"]
    list_keys = ["slots", "platforms", "handlers"]
    if not set(test_def.keys()) == set(string_keys + list_keys + ["when"]):
        return False
    for key in string_keys:
        if not isinstance(test_def[key], str):
            return False
    for key in list_keys:
        if not isinstance(test_def[key], list) or len(test_def[key]) == 0:
            return False
    if type(test_def["when"]) not in (str, list) or len(test_def["when"]) == 0:
        return False

    return True


def get_test_from_yaml(options):
    scheduled_tests = get_test_schedule()
    assert options in scheduled_tests.keys(), f"Requested test {options} was not found"
    if valid_test_definition(st := scheduled_tests[options]):
        return st
    raise ValueError(f"Invalid test definition for the {options}")


def get_last_build_id(slot):
    return 1


def get_test_cmds(test_definition, slot=None, slot_build_id=None, platform=None):
    cmds = []
    if slot:
        slots = [slot]
    else:
        slots = test_definition["slots"]
    if platform:
        platforms = [platform]
    else:
        platforms = test_definition["platforms"]
    for slot in slots:
        if not slot_build_id:
            slot_build_id = get_last_build_id(slot)
        for platform in platforms:
            if "--nightly" in test_definition["command"]:
                install_cmd = [
                    "echo",
                    "Running the LHCbPR test using the nightly build from cvmfs",
                ]
                lbrun_cmd = test_definition["command"]
                lbrun_cmd = (
                    lbrun_cmd.replace("{platform}", platform)
                    .replace("{slot}", f"{slot}/{slot_build_id}")
                    .replace("{options}", test_definition["options"])
                    .split()
                )
            else:
                install_cmd = [
                    "lbn-install",
                    "--verbose",
                    "--flavour",
                    "nightly",
                    "--dest",
                    "build",
                    "--projects",
                    test_definition["project"],
                    "--platforms",
                    platform,
                    "--with-build-dir",
                    slot,
                    slot_build_id,
                ]
                lbrun_cmd = test_definition["command"]
                lbrun_cmd = (
                    lbrun_cmd.replace("{platform}", platform)
                    .replace("{options}", test_definition["options"])
                    .split()
                )
            handler_cmd = ["running", " ".join(test_definition["handlers"]), "handlers"]
            cmds.append(
                {"install": install_cmd, "lbrun": lbrun_cmd, "handler": handler_cmd}
            )
    return cmds
