from lbprschedule.utils import valid_test_definition


def test_valid_test_definition():
    test_def = None
    assert not valid_test_definition(test_def)
    test_def = {}
    assert not valid_test_definition(test_def)
    test_def = {
        "project": "Moore",
        "command": "lb-run",
        "slots": ["lhcb-head", "lhcb-master"],
        "platforms": ["nehalem", "skylake"],
        "handlers": ["myhandler", "yourhandler"],
        "host_label": "avx512",
        "when": "daily",
        "options": "prconfig",
    }
    assert valid_test_definition(test_def)
    test_def["extra"] = ""
    assert not valid_test_definition(test_def)
    test_def.pop("extra")
    test_def["when"] = ["Mon"]
    assert valid_test_definition(test_def)
    test_def["when"] = []
    assert not valid_test_definition(test_def)
    test_def["when"] = ""
    assert not valid_test_definition(test_def)
    test_def["when"] = "daily"
    list_keys = ["slots", "platforms", "handlers"]
    for key in list_keys:
        test_def[key] = []
        assert not valid_test_definition(test_def)
        test_def[key] = "nehalem"
        assert not valid_test_definition(test_def)
        test_def[key] = ["dummy"]
        assert valid_test_definition(test_def)
    string_keys = ["project", "command", "host_label", "options"]
    for key in string_keys:
        test_def.pop(key)
        assert not valid_test_definition(test_def)
        test_def[key] = "dummy"
